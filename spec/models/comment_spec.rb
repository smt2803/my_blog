require 'rails_helper'

describe Comment do

  it { should belong_to (:user) }
  it { should belong_to (:post) }
  it { should validate_presence_of (:comment) }
  it { should validate_length_of(:comment).is_at_most(65535) }
end
